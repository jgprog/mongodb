<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>No SQL - TP</title>

    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span ></span>
          </button>
          <a class="navbar-brand" href="#">!SQL</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li <?php if($page == "home") echo 'class="active"'; ?> ><a href="index.php">Home</a></li>
            <li <?php if($page == "consult") echo 'class="active"'; ?> ><a href="consult.php">Get</a></li>
            <li <?php if($page == "insert") echo 'class="active"'; ?> ><a href="insert.php">Insert</a></li>
            <li <?php if($page == "search") echo 'class="active"'; ?> ><a href="search.php">Search</a></li>
            <li <?php if($page == "Classement") echo 'class="active"'; ?> ><a href="classement.php">Classement</a></li>
             <li <?php if($page == "succes") echo 'class="active"'; ?> ><a href="Succes.php">Succes</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">