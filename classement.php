<?php $page ="Classement"; ?>
<?php include('./assets/header.php'); ?>


<ol class="breadcrumb">
  	<li><a href="./index.php">Mongo DB</a></li>
  	<li>Classement</li>
</ol>

<script>
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>

<?php


// Config
$dbhost = 'localhost';
$dbname = 'mydb';
// Connect to test database
$m = new Mongo();
$db = $m->$dbname;
// select the collection
$collection = $db->shows;

$resulInArray = array();

$retval = $collection->distinct('nom');
for ($i = 0; $i < count($retval); $i++)
{
	$cursor = $collection->find(array('nom' =>  $retval[$i]));
	foreach ($cursor as $obj)
	{
		$stringName = $obj['nom'];
	}
	$temp = iterator_to_array($cursor);
	$resulInArray[$stringName] = count($temp, 0);
}
arsort($resulInArray);

/*
$retval = $collection->distinct('nom');
$retvalForJeu = $collection->distinct('jeu');
for ($i = 0; $i < count($retval); $i++)
{
	$cursor = $collection->find(array('nom' =>  $retval[$i]),array( 'jeu' => $retvalForJeu[$i]));
	foreach ($cursor as $obj)
	{
		$stringName = $obj['nom'];
	}
	$temp = iterator_to_array($cursor);
	$resulInArray[$stringName] = count($temp, 0);
}
var_dump($resulInArray);
arsort($resulInArray);
*/

$m->close(); 
?>

<h1> Retrouvez tous les classements</h1><br/><br/>

<ul class="nav nav-tabs" id="myTab">
  <li class="active"><a href="#home" data-toggle="tab">Globaux</a></li>
  <!--<li><a href="#profile" data-toggle="tab" >Par jeux</a></li>-->
</ul>

<div class="tab-content">
  <div class="tab-pane active" id="home"><br/>
  		<table class="table table-striped">
  		<tr><th>Nom du joueur</th><th>Nombre de succès cumulés</th></tr>
  		<?php
  		foreach($resulInArray as $key => $value)
  		{
  			echo '<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
  		}
  		?>
  		 </table>
  </div>
  
  
  <!--<div class="tab-pane" id="profile"><br/>
  	<p>Par jeu</p>
  </div>-->
</div>


<?php include ('assets/footer.php'); ?>