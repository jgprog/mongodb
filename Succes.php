<?php $page ="succes"; ?>
<?php include('./assets/header.php'); ?>


<ol class="breadcrumb">
  	<li><a href="./index.php">Mongo DB</a></li>
  	<li>Tous les succès</li>
</ol>

<?php
// Config
$dbhost = 'localhost';
$dbname = 'mydb';
// Connect to test database
$m = new Mongo();
$db = $m->$dbname;
// select the collection
$collection = $db->shows;


//On obtient une liste de tous les jeux
$retval = $collection->distinct('jeu');
$ArraySucces = array();

for ($i = 0; $i < count($retval); $i++) 
{
	$cursor = $collection->find(array('jeu' =>  $retval[$i]));
	$resul = $resul ."<h3>".$retval[$i]." </h3><br /><table class=\"table table-striped\">";
	
	foreach ($cursor as $obj)
	{
		if(in_array($obj['badge'],$ArraySucces) == false)
		{
			$resul = $resul."<tr><td>".$obj['badge']."</td></tr>";
			array_push($ArraySucces,$obj['badge']);
		}
		
	}
	$ArraySucces = array();
	$resul = $resul . "</table><br />";

}
	
$m->close(); 
?>
<h1>Tous les succès pour tous les jeux</h1>

<?php
echo $resul;
?>

<?php include ('assets/footer.php'); ?>