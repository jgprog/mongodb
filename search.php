<?php $page ="search"; ?>
<?php include('./assets/header.php'); ?>

<ol class="breadcrumb">
  	<li><a href="./index.php">Mongo DB</a></li>
  	<li>Recherche de tous les badges</li>
</ol>

<div class="starter-template">
	<h1>Voir les stats</h1>
</div>
<small>Saisir le bon nom du joueur</small>
<br/><br/>


<form action="search.php"  role="form" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Nom du joueur</label>
    <input name="name" class="form-control" placeholder="Nom du joueur" value="<?php echo $_POST['name']; ?>">
  </div>
  <input name="submit" type="submit" class="btn btn-default"></input>
</form>

<script>
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>

<?php

if(isset($_POST['submit']))
{
// Config
$dbhost = 'localhost';
$dbname = 'mydb';
// Connect to test database
$m = new Mongo();
$db = $m->$dbname;
// select the collection
$collection = $db->shows;
	
	
//Un seul algo pour les deux :)
$retval = $collection->distinct('jeu');
$previousgame = null;
$nbSuccessOnNone =  "";
$tabResult = "";
$ArraySucces = array();
$PlayGame = array();
for ($i = 0; $i < count($retval); $i++) 
{
	$cursor = $collection->find(array('jeu' =>  $retval[$i]));
	foreach ($cursor as $obj)
	{
		if(in_array($obj['badge'],$ArraySucces) == false)
		{
			if($obj['nom'] == $_POST['name'])
			{
				if($previousgame == null)
				{
					$nbSuccessOnNone =  $nbSuccessOnNone . '<table class="table table-striped"><br /><h3>'.$obj['jeu'].'</h3><br />';
					$tabResult = $tabResult . '<table class="table table-striped"><br /><h3>'.$obj['jeu'].'</h3><br />';
				}
				else if($previousgame != $obj['jeu'] )
				{
					$nbSuccessOnNone =  $nbSuccessOnNone .  '</table><table class="table table-striped"><br /><h3>'.$obj['jeu'].'</h3><br />';
					$tabResult = $tabResult . '</table><table class="table table-striped"><br /><h3>'.$obj['jeu'].'</h3><br />';
				}
				array_push($PlayGame,$obj['jeu']);
			}
		
		
			if($obj['nom'] != $_POST['name'])
			{	
				if(in_array($obj['jeu'],$PlayGame)) 
				{
					$nbSuccessOnNone =  $nbSuccessOnNone .  '<tr class="danger" ><td>'.$obj['badge'].'</td></tr>';
				}
			}
			else
			{
				$nbSuccessOnNone =  $nbSuccessOnNone .  '<tr class="success" ><td>'.$obj['badge'].'</td></tr>';
				$tabResult =  $tabResult .'<tr><td>'.$obj['badge'].'</td></tr>';
			}
			array_push($ArraySucces,$obj['badge']);
			$previousgame = $obj['jeu'];
		}	
	}
	$ArraySucces = array();
}
$tabResult = $tabResult . '</table>';
$nbSuccessOnNone =  $nbSuccessOnNone . '</table>';
$m->close();
?>
<br /><br />
<ul class="nav nav-tabs" id="myTab">
  <li class="active"><a href="#home" data-toggle="tab">Tous les jeux</a></li>
  <li><a href="#profile" data-toggle="tab" >Succès / Manquant</a></li>
  
</ul>

<div class="tab-content">
  <div class="tab-pane active" id="home"><br/>
  		<?php echo $tabResult; ?>
  </div>
  
  
  <div class="tab-pane" id="profile">  
  	<?php  echo $nbSuccessOnNone; ?>  
  </div>
</div>
<?php 
}
?>
<?php include ('assets/footer.php'); ?>