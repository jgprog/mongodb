<?php $page ="consult"; ?>
<?php include('./assets/header.php'); ?>

<ol class="breadcrumb">
  	<li><a href="./index.php">Mongo DB</a></li>
  	<li>Consultation de la liste des joueurs</li>
</ol>

<div class="starter-template">
	<h1>Liste des joueurs dans la base</h1>
	<p class="lead">Ici la consultation des différents joueurs</p>
</div>
<table class="table table-striped">
<tr>
<th>Nom</th>
<th>Badge</th>
<th>Nom du jeu</th>
</tr>
<?php 

// Config
$dbhost = 'localhost';
$dbname = 'mydb';

// Connect to test database
$m = new Mongo();

$db = $m->$dbname;
// select the collection
$collection = $db->shows;

// pull a cursor query
$cursor = $collection->find();

    // How many results found
    $num_docs = $cursor->count();

    if( $num_docs > 0 )
    {
        // loop over the results
        foreach ($cursor as $obj)
        {
        	echo '<tr><td>';
            echo $obj['nom'] . "</td><td>";
            echo $obj['badge']."</td><td>";
            echo $obj['jeu']."</td></tr>";
        }
    }
    else
    {
        // if no products are found, we show this message
        echo "<tr><td>Pas de joueurs</td><td></td><td></td></tr>";
    }
$m->close();    
?>
</table>
<?php include ('assets/footer.php'); ?>
