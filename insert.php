<?php $page ="insert"; ?>
<?php include('./assets/header.php'); ?>

<ol class="breadcrumb">
  	<li><a href="./index.php">Mongo DB</a></li>
  	<li>Insertion d'un nouveau joueur et badge</li>
</ol>

<div class="starter-template">
	<h1>Ajout d'un joueur et d'un badge</h1>
</div>
<small>Veuillez noter qu'il n'y a aucun contrôle des champs</small>
<br/><br/>
<?php

if(isset($_POST['submit']))
{
// Config
$dbhost = 'localhost';
$dbname = 'mydb';

// Connect to test database
$m = new Mongo();

$db = $m->$dbname;
// select the collection
$collection = $db->shows;

$badge = $_POST['badge'];
$name = $_POST['name'];
$jeu = $_POST['jeu'];

$product = array(
                        'nom' => $name,
                        'badge' => $badge,
                        'jeu' => $jeu,
                        );
$collection->insert( $product );
$m->close(); 
?>
<div class="alert alert-success">. Ajout terminé ! Vous pouvez retrouver le résultat sur cette <a href="consult.php">page</a> .</div>
<?php
}
?>

<form action="insert.php"  role="form" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Nom du joueur</label>
    <input name="name" class="form-control" placeholder="Nom du joueur">
  </div>
  <div class="form-group">
    <label>Badge</label>
    <input name="badge" class="form-control" placeholder="Badge level">
  </div>
  <div class="form-group">
    <label>Nom du jeu</label>
    <input name="jeu" class="form-control" placeholder="Nom du jeu">
  </div>
  <input name="submit" type="submit" class="btn btn-default"></input>
</form>


<?php include ('assets/footer.php'); ?>